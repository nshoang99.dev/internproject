const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const AccountSchema = new Schema({
  name: {type: String},
  password: {type: String},
  password_salt: {type: String},
  password_algorithm: {type: String},
  fullname: {type: String},
  email: {type: String},
  numberphone: { type: String},
  employee_id: {type: String},
  org_code: {type: String},
  status: {type: Number},
  created_on:  { type: Date, default: new Date },
  created_by: {type: String},
  modified_on:{type: Date},
  modified_by:{type: String}
});

module.exports = mongoose.model('Account',AccountSchema);