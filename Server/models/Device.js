const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DeviceSchema = new Schema({
  name: {type: String},
  imei: {type: String},
  sery_sim: {type: String},
  org_code: {type: String},
  active: {type: Number},
  created_by: {type: String},
  created_on:  {type: Date, default: new Date },
  modified_on:{type: Date},
  modified_by:{type: String}
});

module.exports = mongoose.model('Device',DeviceSchema);