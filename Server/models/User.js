const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const UserSchema = new Schema({
  user: {type: String},
  email: {type: String},
  password: {type: String},
  token: {type: String},
  fullname: {type: String},
  type: { type: String},
  created:  { type: Date, default: new Date }
});

module.exports = mongoose.model('User',UserSchema);