var express = require('express');
const { Schema } = require('mongoose');
var router = express.Router();
const DeviceSchema = require('../models/Device');

router.post('/create-device',async(req,res,next)=>{
    const{name,imei,sery_sim,org_code,active,created_on,created_by,modified_on,modified_by} = req.body
    if(!name || !imei || !sery_sim  )
    {
        return res.json({
            message: " thong tin chua day du",
            code: '0'
        })
    }

    try {
        let device1 = {
            name,
            imei,
            sery_sim,
            org_code,
            active,
            created_by,
            created_on,
            modified_on,
            modified_by
        }
        const currentDevice = await DeviceSchema
        .findOne({imei})
        if(currentDevice)
        {
            return res.json({
                message: 'thiet bi da ton tai',
                code: '2'
            })
        }

        const Device = new DeviceSchema(device1)
        console.log("device",device1)
        await Device.save()
        res.json({
            message:"created",
            code: 1
        })
    } catch (error) {
        console.log(error)
        res.json({
            message:'0'
        })
    }
})

router.get('/get-alldevice',async(req,res,next)=>{
    try {
        const getAlldevice = await DeviceSchema
        .find()
        return res.json({
            device: getAlldevice,
            code: '1'
        })
        
    } catch (error) {
        res.json({
            message:'0'
        })
    }
})

router.get('/get-devicebyid',async(req,res,next)=>{
    
    try {
        const {id} = req.body
        if(!id){
            return res.json({
                message:'thiet bi khong ton tai',
                code: '0'
            })
        }
        const getDevicebyid = await DeviceSchema
        .findOne({_id:id})
        res.json({
            device: getDevicebyid,
            code: 1
        })
    } catch (error) {
        console.log(error)
        res.json({
            message:'0'
        })
    }
})

router.post('/update-device',async(req,res,next)=>{
    try {
        const {id} = req.body
        if(!id){
            return res.jso({
                message:'thiet bi k ton tai',
                code: '0'
            })
        }

        const updateDevice ={
            name: req.body.name,
            imei: req.body.imei,
            sery_sim: req.body.sery_sim,
            org_code: req.body.org_code,
            active: req.body.active,
            created_by: req.body.created_by,
            created_on: req.body.created_on,
            modified_on: req.body.modified_on,
            modified_by: req.body.modified_by
        }
        await DeviceSchema
        .findOneAndUpdate({_id:id},updateDevice,{upsert:true})
        res.json({
            device: updateDevice,
            code:1
        })
    } catch (error) {
        console.log(error)
        res.json({
            message:'0'
        })
    }
})

router.post('/delete-device',async(req,res,next)=>{
try {
    const {id}=req.body
    const deleteDevice = await DeviceSchema
    .deleteOne({_id:id})
    return res.json(
        {
        message:'deleted',
        code: 1
    }
    )
} catch (error) {
    console.log(error)
    res.json({
        message:'0'
    })
}
})

module.exports = router;

