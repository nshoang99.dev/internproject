var express = require('express');
const { Schema } = require('mongoose');
var router = express.Router();
const UserSchema = require('../models/User')


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/register', async(req, res, next) => {
  const {user, fullname, email, password} = req.body
  console.log("fullname",fullname);
  if(!user || !email) {
    return res.json({
      message: 'Phai nhap du lieu',
      code: 4
    })
  }
  if(!fullname) {
    return res.json({
      message: 'Phai nhap fullname',
      code: 3
    });
  }
  
  try {
    
    let user1 = {
      user,
      fullname,
      email,
      password,
    }
    const currentUser = await UserSchema
      .findOne({user,password})
    if(currentUser) {
      return res.json({
        message: 'User ton tai',
        code: 2
      });
    }
      const User = new UserSchema(user1)
    console.log("user",user1)
    await User.save()
    res.json({
      message: 'Success',
      code: 1
    });
  } catch (error) {
    console.log(error)
    res.json({
      message: 'Error',
      code: 0
    });
  }
});
router.post('/login',async(req, res, next)=>{

  try{
    const {user,password} = req.body
    if(!user || !password)
    {
      return res.json({
        message: 'bat buoc phai nhap du',
        code: '0'
    })

    }
    const checkUser = await UserSchema
    .findOne({user,password})
    console.log(checkUser);
      return res.json(
        {
          user: checkUser,
          code: "1"
        }
      )

  } catch(err){
    console.log(err)
    res.json({
        message:"0"
      });
  }
});

router.get('/get-users',async(req,res,next)=>{
try {
  const getUser = await UserSchema
  .find()
  console.log(getUser);
  return res.json(
    {
      user: getUser,
      code: '1'
    }
  )
  
} catch (error) {
  console.log(error)
  res.json({
    message:'0'
  });
}
})

router.get('/get-userbyid',async(req,res,next)=>{
  try {
    const {id} = req.body
    if(!id)
    {
      return res.json(
        {
          message:"khong co id",
          code:"0"
        }
      )
    }
    const getUserbyid = await UserSchema
    .findOne({_id:id})
    console.log(getUserbyid);
    return res.json(
      {
        user: getUserbyid,
        code: "1"
      }
    );
  } catch (error) {
    console.log(error)
    res.json({
      message:'0'
    });
  }
})

router.post('/update-user',async(req,res,next)=>{
  try {
    const{id,user} = req.body
    const updateUser = await UserSchema
    .findOneAndUpdate({_id:id},{$set: {user: user}},{new: true})
    console.log(updateUser);
    return res.json({
      user: updateUser,
      code: "1"
    })
  } catch (error) {
    console.log(error)
    res.json({
      message:'0'
    })
  }
})

router.post('/update-fullname',async(req,res,next)=>{
  try {
    const{id,fullname}= req.body
    const updateFullname=await UserSchema
    .findOneAndUpdate({_id:id},{$set:{fullname:fullname}},{new:true})
    console.log(updateFullname);
    return res.json({
      fullname: updateFullname,
      code: "1"
    })
  } catch (error) {
    console.log(error)
    res.json({
      message:'0'
    })
  }
})

router.post('/update-email',async(req,res,next)=>{
  try {
    const{id,email}= req.body
    const updateEmail=await UserSchema
    .findOneAndUpdate({_id:id},{$set:{email:email}},{new:true})
    console.log(updateEmail);
    return res.json({
      email: updateEmail,
      code: "1"
    })
  } catch (error) {
    console.log(error)
    res.json({
      message:'0'
    })
  }
})

router.post('/update-password',async(req,res,next)=>{
  try {
    const{id,password}= req.body
    const updatePassword=await UserSchema
    .findOneAndUpdate({_id:id},{$set:{password:password}},{new:true})
    console.log(updatePassword);
    return res.json({
      password: updatePassword,
      code: "1"
    })
  } catch (error) {
    console.log(error)
    res.json({
      message:'0'
    })
  }
})

router.post('/delete-account',async(req,res,next)=>{
  try {
    const {id} = req.body
    const deleteUser = await UserSchema
    .deleteOne({_id:id})
    return res.json(
      {
        message:"deleted",
        code:'1'
      }
    )
  } catch (error) {
    console.log(error)
    res.json({
      message:'0'
    })
  }
})

module.exports = router;

