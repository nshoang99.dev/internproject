var express = require('express');
const { Schema } = require('mongoose');
var router = express.Router();
const AccountSchema = require('../models/Account');

router.post('/create-account',async(req,res,next)=>{

    const {name,password,password_salt,password_algorithm,fullname,email,numberphone,employee_id,org_code,status,created_by,modified_on,modified_by}=req.body

    console.log("name", name)
    console.log("pass", password)
    console.log("fullname", fullname)
    console.log("email", email)
    if(!name || !password || !fullname || !email )
    {
       return res.json({
            message:" du lieu bat buoc can nhap",
            code: '0'
        })
    }
    try {
        let account1 = {
            name,
            password,
            password_salt,
            password_algorithm,
            fullname,
            email,
            numberphone,
            employee_id,
            org_code,
            status,
            created_by,
            modified_on,
            modified_by
          }
          
          const currentAccount = await AccountSchema
          .findOne({name,password})
          if(currentAccount)
          {
              return res.json({
                  message:"tai khoan da ton tai",
                  code: '2'
              })
          }

        const Account = new AccountSchema(account1)
        console.log('account',account1)
        await Account.save()
        res.json({
            message: 'created',
            code: 1
        });
    } catch (error) {
        console.log(error)
        res.json({
            message:'0'
        })
    }
    
    
})

router.get('/get-allaccount',async(req,res,next)=>{
    try {
        const getAllaccount = await AccountSchema
        .find()
        console.log(getAllaccount)
        return res.json({
            account: getAllaccount,
            code: '1',
        })
    } catch (error) {
        console.log(error)
        res.json({
            message:'0'
        });
    }
})

router.post('/get-accountbyid',async(req,res,next)=>{
    try {
        const {id}=req.body
        if(!id){
            return res.json({
                message:" id khong ton tai",
                code: 0
            });
        }
    
        const getAccountbyid = await AccountSchema
        .findOne({_id:id})
        console.log(getAccountbyid)
        res.json({
            account: getAccountbyid,
            code: '1'
        })
    } catch (error) {
        console.log(error)
        res.json({
            message:'0'
        });
    }
    })


    router.post('/delete-account',async(req,res,next)=>{
        try {
          const {id} = req.body
          const deleteAccount = await AccountSchema
          .deleteOne({_id:id})
          return res.json(
            {
              message:"deleted",
              code: 1
            }
          )
        } catch (error) {
          console.log(error)
          res.json({
            message:'0'
          })
        }
      })


router.post('/update-account', async (req,res) => {
    
    try {
        const{id} = req.body
        if(!id)
        {
              return res.json({
                message:'id khong ton tai',
                code: '0'
            })
        }
               
        const updateAccount = {
        name: req.body.name,
        password: req.body.password ,
        password_salt: req.body.password_salt ,
        password_algorithm: req.body.password_algorithm ,
        fullname: req.body.fullname ,
        email: req.body.email ,
        numberphone: req.body.numberphone ,
        employee_id: req.body.employee_id ,
        org_code: req.body.org_code,
        status: req.body.status ,
        created_on: req.body.created_on ,
        created_by: req.body.created_by ,
        modified_on: req.body.modified_on ,
        modified_by: req.body.modified_by
        }
        await AccountSchema.findOneAndUpdate({_id:id},updateAccount,{upsert:true})
        console.log(updateAccount)
        res.json({
            account: updateAccount,
            code: 1
        })
    } catch (error) {
        console.log(error)
        res.json({
            message:'0'
        })
    }
})
module.exports = router;