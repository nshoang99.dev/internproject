export const ACCOUNT_FETCH = 'account@fetch'
export const ACCOUNT_SUCCESS = 'account@success'
export const ACCOUNT_FAILURE = 'account@failure'

export const ACCOUNT_UPDATE = 'account@update'
export const ACCOUNT_UPDATE_SUCCESS = 'account@update@success'
export const ACCOUNT_UPDATE_FAILURE = 'account@update@failure'
export const ACCOUNT_UPDATE_RESET = 'account@update@reset'

export const ACCOUNT_CREATED = 'account@created'
export const ACCOUNT_CREATED_SUCCESS = 'account@created@success'
export const ACCOUNT_CREATED_FAILURE = 'account@created@failure'
export const ACCOUNT_CREATED_RESET = 'account@created@reset'


export const ACCOUNT_DELETE = 'account@delete'
export const ACCOUNT_DELETE_SUCCESS = 'account@deletd@success'
export const ACCOUNT_DELETE_FAILURE = 'account@delet@failure'
export const ACCOUNT_DELETE_RESET = 'account@delet@reset'
export const fetchAccountById = (action) => {
    return {
        type: ACCOUNT_FETCH,
        ...action
    }
}

export const updateAccount = (action) => {
    return {
        type: ACCOUNT_UPDATE,
        ...action
    }
}

export const deleteAccount = (action) => {
    return {
        type: ACCOUNT_DELETE,
        ...action
    }
}


export const createdAccount = (action) => {
    return {
        type: ACCOUNT_CREATED,
        ...action
    }
}