export const CREATED_SAVE = 'user@created/save'
export const CREATED_SUCCESS = 'user@created/success'
export const CREATED_FAIL = 'user@created/fail'

export const MODIFIED_SAVE = 'user@modifed/save'
export const MODIFIED_SUCCESS = 'user@modifed/success'
export const MODIFIED_FAIL = 'user@modifed/fail'

export const GET_SAVE ='user@get/save'
export const GET_SUCCESS = 'user@get/success'
export const GET_FAIL = 'user@get/fail'


export const CREATEACCOUNT_SAVE = 'account@created/save'
export const CREATEACCOUNT_SUCCESS = 'account@created/success'
export const CREATEACCOUNT_FAIL = 'account@created/fail'
export const CREATEACCOUNT_RESET = 'account@created/reset'

export const UPDATEACCOUNT_SAVE = 'account@updated/save'
export const UPDATEACCOUNT_SUCCESS = 'account@updated/success'
export const UPDATEACCOUNT_FAIL = 'account@updated/fail'
export const UPDATEACCOUNT_RESET = 'account@updated/reset'

export const DELETEACCOUNT_SAVE = 'account@delete/save'
export const DELETEACCOUNT_SUCCESS = 'account@delete/success'
export const DELETEACCOUNT_FAIL = 'account@delete/fail'
export const DELETEACCOUNT_RESET = 'account@delete/reset'


///





export const createdUser = (action) => {
    return {
        type: CREATED_SAVE,
        ...action
    }
}

export const createdAccount = (action) => {
    return {
        type: CREATEACCOUNT_SAVE,
        ...action
    }
}

export const modifyUser =(action)=>{
    return{
        type: MODIFIED_SAVE,
        ...action
    }
}



export const updateAccount =(action)=>{
    return{
        type: UPDATEACCOUNT_SAVE,
        ...action
    }
}

export const deleteAccount =(action)=>{
    return{
        type: DELETEACCOUNT_SAVE,
        ...action
    }
}


export const getAllUser =(action)=>{
    return{
        type: GET_SAVE,
        ...action
    }
}


