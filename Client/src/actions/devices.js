export const GETDEVICE_SAVE ='device@get/save'
export const GETDEVICE_SUCCESS = 'device@get/success'
export const GETDEVICE_FAIL = 'device@get/fail'

export const CREATEDEVICE_SAVE = 'device@create/save'
export const CREATEDEVICE_SUCCESS = 'device@create/success'
export const CREATEDEVICE_FAIL = 'device@create/fail'

export const UPDATEDEVICE_SAVE = 'device@update/save'
export const UPDATEDEVICE_SUCCESS = 'device@update/success'
export const UPDATEDEVICE_FAIL = 'device@update/fail'

export const DELETEDEVICE_SAVE = 'device@delete/save'
export const DELETEDEVICE_SUCCESS = 'device@delete/success'
export const DELETEDEVICE_FAIL = 'device@delete/fail'

export const getAllDevice =(action)=>{
    return{
        type: GETDEVICE_SAVE,
        ...action
    }
}

export const createDevice = (action) => {
    return {
        type: CREATEDEVICE_SAVE,
        ...action
    }
}

export const updateDevice =(action)=>{
    return{
        type: UPDATEDEVICE_SAVE,
        ...action
    }
}

export const deleteDevice =(action)=>{
    return{
        type: DELETEDEVICE_SAVE,
        ...action
    }
}


