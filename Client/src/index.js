
import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import { Provider } from 'react-redux';
// Thêm applyMiddleware để redux có thể sử dụng redux-saga
import { createStore, applyMiddleware } from 'redux';
// Gọi function createSagaMiddle
import createSagaMiddleware from 'redux-saga';



// core components
import Admin from "layouts/Admin.js";
import RTL from "layouts/RTL.js";

import "assets/css/material-dashboard-react.css?v=1.9.0";


import {rootSaga} from './sagas'
import allReducers from './reducers'

const hist = createBrowserHistory();


const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  allReducers,
  applyMiddleware(sagaMiddleware),
);

// Để có thể lắng nghe action thì phải chạy sagaMiddleware
sagaMiddleware.run(rootSaga);

const AppInit = () => {
  return(
    <Router history={hist}>
    <Switch>
      <Route path="/admin" component={Admin} />
      <Route path="/rtl" component={RTL} />
      <Redirect from="/" to="/admin/dashboard" />
    </Switch>
  </Router>
  )
}
ReactDOM.render(
  <Provider store={store}>
    <AppInit />
  </Provider>,
  document.getElementById("root")
);
