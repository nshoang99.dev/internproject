

// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";

import Notifications from "@material-ui/icons/Notifications";

import Language from "@material-ui/icons/Language";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
import TableList from "views/TableList/TableList.js";
import Typography from "views/Typography/Typography.js";

import NotificationsPage from "views/Notifications/Notifications.js";


import RTLPage from "views/RTLPage/RTLPage.js";
import Device from "views/Device/Device";
import Account from "views/Account/Account";
import Task from "views/Task/Task";
import {  CheckCircleOutline, PhoneIphone } from "@material-ui/icons";

import AccountAdd from 'views/Account/AccountAdd'





const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/device",
    name: "Device",
    icon: PhoneIphone,
    component: Device,
    layout: "/admin"
  },
  {
    path: "/account",
    name: "Account",
    icon: Person,
    component: Account,
    layout: "/admin"
  },
  {
    path: "/accountAdd/:id",
    name: "Account1",
    icon: Person,
    show: true,
    component: AccountAdd,
    layout: "/admin"
  },
  {
    path: "/task",
    name: "Task",
    icon: CheckCircleOutline,
    component: Task,
    layout: "/admin"
  },
  { 
    path: "/user",
    name: "User Profile",
    icon: Person,
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/table",
    name: "Table List",
    icon: "content_paste",
    component: TableList,
    layout: "/admin"
  },  
   {
    path: "/typography",
    name: "Typography",
    rtlName: "طباعة",
    icon: LibraryBooks,
    component: Typography,
    layout: "/admin"
  }, 
  {
    path: "/notifications",
    name: "Notifications",
    rtlName: "إخطارات",
    icon: Notifications,
    component: NotificationsPage,
    layout: "/admin"
  }, 
   {
    path: "/rtl-page",
    name: "RTL Support",
    rtlName: "پشتیبانی از راست به چپ",
    icon: Language,
    component: RTLPage,
    layout: "/rtl"
  }, 
 /*  {
    path: "/upgrade-to-pro",
    name: "Upgrade To PRO",
    rtlName: "التطور للاحترافية",
    icon: Unarchive,
    component: UpgradeToPro,
    layout: "/admin"
  } */

  
];



export default dashboardRoutes;
