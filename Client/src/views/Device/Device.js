import React,{useEffect, useState} from "react";
import { getAllDevice } from "actions/devices";

import {useDispatch, useSelector} from 'react-redux';
import CardFooter from "components/Card/CardFooter";
import Button from "components/CustomButtons/Button.js";

import Modal from 'react-modal';
import {  updateDevice } from "actions/devices";

import { createDevice } from "actions/devices";
import { deleteDevice } from "actions/devices";



const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

export default function Device() {
  
  const dataReducer = useSelector((state) => state.getDeviceReducer)
  const deviceReducer = useSelector((state) => state.deviceReducer)
 
 
  const dispatch = useDispatch();
  const [arr, setArr] = useState([])
  const [name, setName] = useState('')
  const [imei, setImei] = useState('')
  const [org_code, setOrgCode] = useState('')
  const [sery_sim, setSerySim] = useState('')
  const [active, setActive] = useState('')
  const [created_by, setCreatedBy] = useState('')
  const [modalIsOpen,setIsOpen] = React.useState(false);
  const [id, setId]= useState('')
  function openModal(item) {
    console.log("item",item)
    setName(item.name)
    setImei(item.imei)
    setOrgCode(item.org_code)
    setSerySim(item.sery_sim)
    setActive(item.active)
    setId(item._id)
    setCreatedBy(item.created_by)
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
   
  }

  function closeModal(){
    if(id) {
      handleUpdateDevice()
    } else {
      handleCreateDevice()

    }
    setIsOpen(false);
  }
 
  useEffect(()=>{
    console.log('deviceReducer',deviceReducer)
  },[deviceReducer.message]) 

  useEffect(() => {
    handleGetDevice()

    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
  useEffect(() => {
    setArr(dataReducer.data ? dataReducer.data : [])

    // eslint-disable-next-line react-hooks/exhaustive-deps

  },[dataReducer.loading]) 

  const handleGetDevice = () => {

    const param = {
      
    }
    dispatch(getAllDevice(param))
  }

   const handleCreateDevice =()=>{
     const param = {
      name: name,
      imei: imei,
      org_code: org_code,
      sery_sim: sery_sim,
      active: active,
      created_by: created_by
    }
     dispatch(createDevice(param))
  }

   const handleUpdateDevice =()=>{
    const param = {
     id:id,
     name: name,
     imei: imei,
     org_code: org_code,
     sery_sim: sery_sim,
     active: active,
     created_by: created_by
   }
    dispatch(updateDevice(param))
 }
 


  const handleDeleteDevice = (id) => {
     const param ={
      id:id
    } 
    dispatch(deleteDevice(param))
  }

const Item = (props) => {
  const {item} = props
  
  return( 
    <tr>
            
            <td>{item?.name ? item.name : ''}</td>
            <td>{item?.imei ? item.imei : ''}</td>
            <td>{item?.org_code ? item.org_code : ''}</td>
            <td>{item?.sery_sim ? item.sery_sim : ''}</td>
            <td style={{ textAlign: 'center' }}>{item?.active ? item.active : '0'}</td>
            <td>{item?.created_by ? item.created_by : ''}</td>
            
            <td>
              <Button color="warning" onClick={()=>openModal(item)} >EDIT</Button>
              <Button color="danger" onClick={()=>handleDeleteDevice(item._id)} >DELETE</Button>
            </td>
          </tr>
  )
}

  return (
    
    <div className="table-responsive">
    <div className="table-wrapper">
      <div className="table-title">
      <div className="row">
          
         
          <CardFooter>
             <Button onClick={openModal} color="danger">ADD</Button>
          </CardFooter>
            
            <CardFooter>
             <Button color="primary" onClick={handleGetDevice} >GET</Button>
            </CardFooter>
              
  
          </div>
      </div>
      <table className="table table-striped table-hover">
        <thead>
          <tr >
            <th>Tên thiết bị</th>
            <th>Imei</th>
            <th>ORG_CODE</th>
            <th>Sery_Sim</th>
            <th style={{ textAlign: 'center' }} > Trạng thái  </th>
            <th>Người tạo</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {
            arr.map(i => {
              return(
                <Item item={i} key={i._id}/>
              )
            })
          }
         
        </tbody>
      </table>
    </div>
    <div>
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >

      <form>
      <div className="form-row">
      <div className="form-group col-md-6">
      <label >Tên thiết bị </label>
      <input value={name} onChange={(e) => setName(e.target.value)} type="text"  className="form-control" id="inputEmail4" placeholder="" />
      </div>
      <div className="form-group col-md-6">
      <label htmlFor="inputPassword4">Imei</label>
      <input value={imei} onChange={(e)=> setImei(e.target.value)} className="form-control"  placeholder="" />
      </div>
      </div>
      <div className="form-row">
      <div className="form-group col-md-6">
      <label htmlFor="inputEmail4">ORG_CODE</label>
      <input type="text" value={org_code} onChange={(e)=> setOrgCode(e.target.value)} className="form-control"  placeholder="" />
      </div>
      <div className="form-group col-md-6">
      <label htmlFor="inputAddress2">Sery_Sim</label>
      <input type="text" value={sery_sim} onChange={(e)=> setSerySim(e.target.value)} className="form-control"  placeholder="" />
      </div>
      </div>
      <div className="form-row">
    <div className="form-group col-md-6">
      <label htmlFor="inputNumber">Tạo bởi</label>
      <input type="text" value={created_by} onChange={(e)=> setCreatedBy(e.target.value)} className="form-control"  />
    </div>
    <div className="form-group col-md-6">
      <label htmlFor="inputState">Trạng thái</label>
      <select id="inputState" value={active} onChange={(e)=> setActive(e.target.value)} className="form-control">
        <option selected>Chọn trạng thái</option>
        <option>0</option>
        <option>1</option>
      </select>
    </div>
  </div>
  <button type="submit" className="btn btn-primary" alert="true" onClick={closeModal} >Lưu</button>
</form>

        </Modal>
      </div>
      
  </div>
   
  );
}

