import React,{useEffect, useState} from "react";
import { getAllUser } from "actions/users";
import {useDispatch, useSelector} from 'react-redux';
import CardFooter from "components/Card/CardFooter";
import Button from "components/CustomButtons/Button.js";

import Modal from 'react-modal';
import { createdAccount, updateAccount } from "actions/users";
import { deleteAccount } from '../../actions/account'
import {Link} from 'react-router-dom'


const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

export default function Account() {
  
  const dataReducer = useSelector((state) => state.getUserReducer)
  const accReducer = useSelector((state) => state.accountReducer)
  const delReducer = useSelector(state => state.deleteAccountReducer)
 
  const dispatch = useDispatch();
  const [arr, setArr] = useState([])
  const [name, setName] = useState('')
  const [pass, setPass] = useState('')
  const [mail, setMail] = useState('')
  const [fullname, setFullname] = useState('')
  const [numberphone, setNumberphone] = useState('')
  const [status, setStatus] = useState('')
  const [modalIsOpen,setIsOpen] = React.useState(false);
  const [id, setId]= useState('')
  function openModal(item) {
    
    setName(item.name)
    setPass(item.password)
    setStatus(item.status)
    setFullname(item.fullname)
    setMail(item.email)
    setId(item._id)
    setNumberphone(item.numberphone)
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
   
  }

  function closeModal(){
    if(id) {
      handleUpdateAccount()
    } else {
      handleCreateAccount()

    }
    setIsOpen(false);
  }
  useEffect(()=>{
    
    if(accReducer.success) {
     const param = {
      //deleteAccountReducer
    }
      dispatch(getAllUser(param))
    }
  },[accReducer.success]) 
  useEffect(()=>{
    if(delReducer.success) {
      const param = {
       //deleteAccountReducer
     }
       dispatch(getAllUser(param))
     }
  },[delReducer.success]) 
  useEffect(() => {
    handleGetUser()

    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
  useEffect(() => {
    setArr(dataReducer.data ? dataReducer.data : [])

    // eslint-disable-next-line react-hooks/exhaustive-deps

  },[dataReducer.data]) 

  const handleGetUser = () => {

    const param = {
      
    }
    dispatch(getAllUser(param))
  }

   const handleCreateAccount =()=>{
     const param = {
      name: name,
      email: mail,
      fullname: fullname,
      password: pass,
      numberphone: numberphone,
      status: status
    }
     dispatch(createdAccount(param))
  }

  const handleUpdateAccount =()=>{
    const param = {
     id:id,
     name: name,
     email: mail,
     fullname: fullname,
     password: pass,
     numberphone: numberphone,
     status: status
   }
    dispatch(updateAccount(param))
    window.location.reload();
 }



  const handleDeleteAccount = (id) => {
    
     const param ={
      id
    } 
    dispatch(deleteAccount(param))
  }

const Item = (props) => {
  const {item} = props
  
  return( 
    <tr>
            
            <td>{item?.name ? item.name : ''}</td>
            <td>{item?.email ? item.email : ''}</td>
            <td>{item?.fullname ? item.fullname : ''}</td>
            <td>{item?.numberphone ? item.numberphone : ''}</td>
            <td style={{ textAlign: 'center' }}>{item?.status ? item.status : '0'}</td>
            
            <td>
              <Link to={`/admin/accountAdd/${item._id}`}>
                <Button color="warning" >EDIT</Button>
              </Link>
              <Button color="danger" onClick={()=>handleDeleteAccount(item._id)} >DELETE</Button>
            </td>
          </tr>
  )
}

  return (
    
    <div className="table-responsive">
    <div className="table-wrapper">
      <div className="table-title">
      <div className="row">
          
         
          <CardFooter>
          {/* <Button onClick={openModal} color="danger">ADD</Button> */}
              <Link to="/admin/accountAdd/-1"><Button onClick={openModal} color="danger">ADD</Button></Link>
          </CardFooter>
            
            <CardFooter>
             <Button color="primary" onClick={handleGetUser} >GET</Button>
            </CardFooter>
              
  
          </div>
      </div>
      <table className="table table-striped table-hover">
        <thead>
          <tr >
            <th>Tên tài khoản</th>
            <th>Email</th>
            <th>Tên đầy đủ</th>
            <th>Số điện thoại</th>
            <th style={{ textAlign: 'center' }} > Trạng thái</th>
            
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {
            arr.map(i => {
              return(
                <Item item={i} key={i._id}/>
              )
            })
          }
         
        </tbody>
      </table>
    </div>
    <div>
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >

      <form>
      <div className="form-row">
      <div className="form-group col-md-6">
      <label >Tên tài khoản </label>
      <input value={name} onChange={(e) => setName(e.target.value)} type="email" className="form-control" id="inputEmail4" placeholder="" />
      </div>
      <div className="form-group col-md-6">
      <label htmlFor="inputPassword4">Mật khẩu</label>
      <input type="password" value={pass} onChange={(e)=> setPass(e.target.value)} className="form-control" id="inputPassword4" placeholder="" />
      </div>
      </div>
      <div className="form-row">
      <div className="form-group col-md-6">
      <label htmlFor="inputEmail4">Mail</label>
      <input type="text" value={mail} onChange={(e)=> setMail(e.target.value)} className="form-control" id="inputAddress" placeholder="" />
      </div>
      <div className="form-group col-md-6">
      <label htmlFor="inputAddress2">Fullname</label>
      <input type="text" value={fullname} onChange={(e)=> setFullname(e.target.value)} className="form-control" id="inputAddress2" placeholder="" />
      </div>
      </div>
      <div className="form-row">
    <div className="form-group col-md-6">
      <label htmlFor="inputNumber">Number Phone</label>
      <input type="text" value={numberphone} onChange={(e)=> setNumberphone(e.target.value)} className="form-control" id="inputCity" />
    </div>
    <div className="form-group col-md-6">
      <label htmlFor="inputState">Trạng thái</label>
      <select id="inputState" value={status} onChange={(e)=> setStatus(e.target.value)} className="form-control">
        <option selected>Chọn trạng thái</option>
        <option>0</option>
        <option>1</option>
      </select>
    </div>
  </div>
  <button type="submit" className="btn btn-primary" alert="true" onClick={closeModal} >Lưu</button>
</form>

        </Modal>
      </div>
      
  </div>
   
  );
}

