import React , {useEffect, useState}from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
} from "reactstrap";
import { useDispatch, useSelector } from 'react-redux';
import { fetchAccountById ,updateAccount, createdAccount } from '../../actions/account';

const AccountAdd = () => {
  let history = useHistory();
  let dispatch = useDispatch();
  let accountId = useSelector(state => state.accountIdReducer)
  let isUpdate = useSelector(state => state.accountUpdatesReducer)
  let isCreated = useSelector(state => state.accountCreatedReducer)
  const { id } = useParams();
  const [fullname, setFullName] = useState('')
  const [userName, setUserName] = useState('')
  const [mail, setMail] = useState('')
  const [phone, setPhone] = useState('')
  const [status, setStatus] = useState(0)
  const [password, setPassword] = useState('')
  useEffect(() => {
    if(id && id !== '-1') {
      fetchDataById(id)
    } else {
      setFullName('')
      setUserName('')
      setMail('')
      setPhone('')
      setStatus('')
      setPassword('')
    }
  },[])

  useEffect(() => {
    if(accountId.success) {
      setFullName(accountId?.data?.fullname ?? '')
      setUserName(accountId?.data?.name ?? '')
      setMail(accountId?.data?.email ?? '')
      setPhone(accountId?.data?.numberphone ?? '')
      setStatus(accountId?.data?.status ?? '')
      setPassword(accountId?.data?.password ?? '')
    }
  },[accountId.success])
  useEffect(() => {
    if(isUpdate.success) {
      return handleSave()
    }
  },[isUpdate.success])
  //isCreated
  useEffect(() => {
    if(isCreated.success) {
      return handleSave()
    }
  },[isCreated.success])
  const handleSave = () => {
      history.push("/admin/account");
  }
  const fetchDataById = (xid) => {
    const param = {
      id: xid
    }
    dispatch(fetchAccountById(param))
  }
  const handle = () => {
    if(id && id !== '-1') {
      updateData()
    } else {
      createdData()
    }
  }
  const createdData = () => {
    const param = {
      name: userName,
      email: mail,
      fullname: fullname,
      numberphone: phone,
      status: status,
      password
    }
    dispatch(createdAccount(param))
  }
  const updateData = () => {
    const param = {
      id,
      name: userName,
      email: mail,
      fullname,
      numberphone: phone,
      status,
      password
    }
    dispatch(updateAccount(param))
  }
    return (
      <div className="content">
        <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <h5 className="title">Edit Profile</h5>
                </CardHeader>
                <CardBody>
                  <Form>
                    
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Ho va ten</label>
                          <Input
                            value={fullname}
                            onChange={(e) => setFullName(e.target.value)}
                            placeholder="Lê Văn A ..."
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <label>Tai khoan</label>
                          <Input
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                            placeholder="Tai khoan"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col md="6">
                        <FormGroup>
                          <label>Mat khau</label>
                          <Input
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            placeholder="Mat khau"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Email</label>
                          <Input
                            value={mail}
                            onChange={(e) => setMail(e.target.value)}
                            placeholder="Email"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="6">
                        <FormGroup>
                          <label>phone</label>
                          <Input
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                            placeholder="Phone"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col md="6">
                        <FormGroup>
                          <label>Trang thai</label>
                          <Input
                            value={status}
                            onChange={(e) => setStatus(e.target.value)}
                            placeholder="Trang thai"
                            type="number"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Button onClick={handle} color="primary" style={{marginRight: 5}}>Save</Button>
                    <Link to={`/admin/account`}>
                    <Button  color="success" style={{marginRight: 5}}>Back</Button>
                    </Link>
                  </Form>
                </CardBody>
              </Card>
            </Col>
            
          </Row>
      </div>
    );
}
export default AccountAdd