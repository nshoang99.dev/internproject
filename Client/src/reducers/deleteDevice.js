import { DELETEDEVICE_FAIL,DELETEDEVICE_SAVE,DELETEDEVICE_SUCCESS} from "../actions/devices"
const initialState = {
	loading: false,
    data: null,
    message: false
};

const deleteDeviceReducer = (state = initialState, action) => {
    switch (action.type){
        case DELETEDEVICE_SAVE:
            return {
                ...state
            }
        case DELETEDEVICE_SUCCESS: 
           
            return {
                ...state,
               message: true
                
            }
        case DELETEDEVICE_FAIL: 
            return {
                ...state,
                connecting: false
            }
        default: 
            return {
                ...state
            }
    }
}

export default deleteDeviceReducer