import { GET_FAIL,GET_SAVE,GET_SUCCESS} from "../actions/users"
const initialState = {
	loading: false,
    data: null
};

const getUserReducer = (state = initialState, action) => {

    switch (action.type){
        case GET_SAVE:
            return {
                ...state
            }
        case GET_SUCCESS: 
           
            return {
                ...state,
                loading: true,
                data: action?.getResult?.data?.account
                
            }
        // case DELETE_SUSSCESS:
        //     if(action.id){
        //          const newdata =  state.data.filter(i=>i._id !== action.id)
        //     }
        //     return {
        //         ...state,
        //         data: newdata
        //     }
        case GET_FAIL: 
            return {
                ...state,
                connecting: false
            }
        default: 
            return {
                ...state
            }
    }
}

export default getUserReducer;