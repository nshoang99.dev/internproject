import { 
    ACCOUNT_DELETE,ACCOUNT_DELETE_FAILURE,
    ACCOUNT_DELETE_RESET,ACCOUNT_DELETE_SUCCESS
} from "../actions/account"
const initialState = {
	loading: true,
    success: false
};

const deleteAccountReducer = (state = initialState, action) => {
    switch (action.type){
        case ACCOUNT_DELETE:
            return {
                ...state,
                loading: true,
    
                success: false
            }
        case ACCOUNT_DELETE_SUCCESS: 
           
            return {
                ...state,
                loading: false,
    
                success: true
            }
        case ACCOUNT_DELETE_FAILURE: 
            return {
                ...state,
                loading: false,
    
                success: false
            }
            case ACCOUNT_DELETE_RESET: 
            return {
                ...state,
                loading: true,
                success: false
    
            }
               
        default: 
            return {
                ...state
            }
    }
}

export default deleteAccountReducer