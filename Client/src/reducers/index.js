import {combineReducers } from 'redux'
import userReducer from './users'
import getUserReducer from './getUser'
import accountReducer from './account'
import updateAccountReducer from './account'
import deleteAccountReducer from './deleteAccount'

import getDeviceReducer from './getDevice'
import deviceReducer from './createDevice'
import updateDeviceReducer from './updateDevice'
import deleteDeviceReducer from './deleteDevice'

import accountIdReducer from './accountById'
import accountUpdatesReducer from './accountUpdate'
import accountCreatedReducer from './accountCreated'
const allReducers =  combineReducers({
    userReducer,
    getUserReducer,
    accountReducer,
    updateAccountReducer,
    deleteAccountReducer,
    getDeviceReducer,
    deviceReducer,
    updateDeviceReducer,
    deleteDeviceReducer,
    ///
    accountIdReducer,
    accountUpdatesReducer,
    accountCreatedReducer
});
export default allReducers;