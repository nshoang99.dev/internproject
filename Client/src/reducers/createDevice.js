import { CREATEDEVICE_FAIL,CREATEDEVICE_SAVE,CREATEDEVICE_SUCCESS} from "../actions/devices"
const initialState = {
	loading: true, // khi gọi createAccout thì sẽ hiện bản đang loading
    data: null,
    message: false, //
    success: false // trạng thái thành côcng
};


const deviceReducer = (state = initialState, action) => {
    switch (action.type){
        case CREATEDEVICE_SAVE:
            return {
                ...state
            }
        case CREATEDEVICE_SUCCESS: 
           
            return {
                ...state,
               message: true,
               success: true, // trạng thái thành công
               loading: false // khi tạo mới thành công thì sẽ k hiện loading nữa
            }
        case CREATEDEVICE_FAIL: 
            return {
                ...state,
                connecting: false,
                success: false, // trạng thái không thành công
                loading: false // khi biết là tạo lỗi thì sẽ k hiện loading nữa
            }
        default: 
            return {
                ...state
            }
    }
}

export default deviceReducer