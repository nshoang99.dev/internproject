import { CREATED_FAIL,CREATED_SAVE,CREATED_SUCCESS} from "../actions/users"
const initialState = {
	loading: false,
    data: null
};



const userReducer = (state = initialState, action) => {
    switch (action.type){
        case CREATED_SAVE:
            return {
                ...state
            }
        case CREATED_SUCCESS: 
           
            return {
                ...state,
                loading: true,
                
            }
        case CREATED_FAIL: 
            return {
                ...state,
                connecting: false
            }
        default: 
            return {
                ...state
            }
    }
}

export default userReducer