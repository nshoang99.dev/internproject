
import { CREATEACCOUNT_FAIL,CREATEACCOUNT_SAVE,CREATEACCOUNT_SUCCESS, CREATEACCOUNT_RESET} from "../actions/users"
const initialState = {
	loading: true, // khi gọi createAccout thì sẽ hiện bản đang loading
    data: null,
    message: false, //
    success: false // trạng thái thành côcng
};


const accountReducer = (state = initialState, action) => {
    switch (action.type){
        case CREATEACCOUNT_SAVE:
            return {
                ...state
            }
        case CREATEACCOUNT_SUCCESS: 
           
            return {
                ...state,
               message: true,
               success: true, // trạng thái thành công
               loading: false // khi tạo mới thành công thì sẽ k hiện loading nữa
            }
        case CREATEACCOUNT_FAIL: 
            return {
                ...state,
                connecting: false,
                success: false, // trạng thái không thành công
                loading: false // khi biết là tạo lỗi thì sẽ k hiện loading nữa
            }
            case CREATEACCOUNT_RESET: 
            return {
                ...state,
                loading: true, // khi gọi createAccout thì sẽ hiện bản đang loading
                data: null,
                message: false, //
                success: false // khi biết là tạo lỗi thì sẽ k hiện loading nữa
            }
        default: 
            return {
                ...state
            }
    }
}

export default accountReducer