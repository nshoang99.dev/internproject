
import { 
    ACCOUNT_UPDATE,ACCOUNT_UPDATE_SUCCESS,ACCOUNT_UPDATE_FAILURE,
    ACCOUNT_UPDATE_RESET

    } from "../actions/account"
const initialState = {
	loading: true,
    success: false // trạng thái thành côcng
};


const accountUpdatesReducer = (state = initialState, action) => {

    switch (action.type){
        case ACCOUNT_UPDATE:
            return {
                ...state,
                loading: true,
                success: false
            }
        case ACCOUNT_UPDATE_SUCCESS: 
           
            return {
                ...state,
                loading: false,
                success: true 
            }
        case ACCOUNT_UPDATE_FAILURE: 
            return {
                ...state,
                connecting: false,
                 
                loading: false 
            }
        case ACCOUNT_UPDATE_RESET: 
            return {
                ...state,
                loading: true,
                success: false
            }
        default: 
            return {
                ...state
            }
    }
}

export default accountUpdatesReducer;