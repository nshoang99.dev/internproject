import { UPDATEDEVICE_FAIL,UPDATEDEVICE_SAVE,UPDATEDEVICE_SUCCESS} from "../actions/devices"
const initialState = {
	loading: false,
    data: null,
    message: false
};

const updateDeviceReducer = (state = initialState, action) => {
    switch (action.type){
        case UPDATEDEVICE_SAVE:
            return {
                ...state
            }
        case UPDATEDEVICE_SUCCESS: 
           
            return {
                ...state,
               message: true
                
            }
        case UPDATEDEVICE_FAIL: 
            return {
                ...state,
                connecting: false
            }
        default: 
            return {
                ...state
            }
    }
}

export default updateDeviceReducer