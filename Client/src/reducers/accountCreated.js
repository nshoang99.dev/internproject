
import { 
    ACCOUNT_CREATED,ACCOUNT_CREATED_FAILURE,ACCOUNT_CREATED_SUCCESS,
    ACCOUNT_CREATED_RESET
    } from "../actions/account"
const initialState = {
	loading: true,
    success: false // trạng thái thành côcng
};


const accountCreatedReducer = (state = initialState, action) => {

    switch (action.type){
        case ACCOUNT_CREATED:
            return {
                ...state,
                loading: true,
                success: false
            }
        case ACCOUNT_CREATED_SUCCESS: 
           
            return {
                ...state,
                loading: false,
                success: true 
            }
        case ACCOUNT_CREATED_FAILURE: 
            return {
                ...state,
                success: false ,
                 
                loading: false 
            }
        case ACCOUNT_CREATED_RESET: 
            return {
                ...state,
                loading: true,
                success: false
            }
        default: 
            return {
                ...state
            }
    }
}

export default accountCreatedReducer;