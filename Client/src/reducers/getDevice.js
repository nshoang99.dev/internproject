import { GETDEVICE_FAIL,GETDEVICE_SAVE,GETDEVICE_SUCCESS} from "../actions/devices"
const initialState = {
	loading: false,
    data: null
};

const getDeviceReducer = (state = initialState, action) => {

    switch (action.type){
        case GETDEVICE_SAVE:
            return {
                ...state
            }
        case GETDEVICE_SUCCESS: 
           
            return {
                ...state,
                loading: true,
                data: action?.getResult?.data?.device
                
            }
        // case DELETE_SUSSCESS:
        //     if(action.id){
        //          const newdata =  state.data.filter(i=>i._id !== action.id)
        //     }
        //     return {
        //         ...state,
        //         data: newdata
        //     }
        case GETDEVICE_FAIL: 
            return {
                ...state,
                connecting: false
            }
        default: 
            return {
                ...state
            }
    }
}

export default getDeviceReducer;