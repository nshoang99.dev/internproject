
import { 
    ACCOUNT_SUCCESS,ACCOUNT_FAILURE,ACCOUNT_FETCH
    } from "../actions/account"
const initialState = {
	loading: true,
    data: {},
    
    success: false // trạng thái thành côcng
};


const accountIdReducer = (state = initialState, action) => {
    switch (action.type){
        case ACCOUNT_FETCH:
            return {
                ...state,
                loading: true,
                data: {},
                success: false
            }
        case ACCOUNT_SUCCESS: 
           
            return {
                ...state,
                loading: true,
                data: action?.data,
                success: true 
            }
        case ACCOUNT_FAILURE: 
            return {
                ...state,
                connecting: false,
                data: {}, 
                loading: false 
            }
        default: 
            return {
                ...state
            }
    }
}

export default accountIdReducer