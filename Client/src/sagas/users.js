
import { put, call, takeLatest } from 'redux-saga/effects';
import { CREATED_FAIL,CREATED_SAVE,CREATED_SUCCESS} from "../actions/users";
import { CREATEACCOUNT_FAIL,CREATEACCOUNT_SAVE,CREATEACCOUNT_SUCCESS, CREATEACCOUNT_RESET} from "../actions/users";
import { UPDATEACCOUNT_FAIL,UPDATEACCOUNT_SAVE,UPDATEACCOUNT_SUCCESS , UPDATEACCOUNT_RESET} from "../actions/users";
import { DELETEACCOUNT_FAIL,DELETEACCOUNT_SAVE,DELETEACCOUNT_SUCCESS, DELETEACCOUNT_RESET} from "../actions/users";
import { createAccountAPI, saveUserAPI,updateAccountAPI,deleteAccountAPI } from '../apis/users';
import {GET_SAVE,GET_SUCCESS,GET_FAIL} from "../actions/users"
import { getAllUserAPI } from '../apis/users'
import { ACCOUNT_UPDATE_RESET,ACCOUNT_CREATED_RESET, 
  ACCOUNT_DELETE_SUCCESS , ACCOUNT_DELETE_RESET,
  ACCOUNT_DELETE_FAILURE,ACCOUNT_DELETE
} from '../actions/account'
function* saveUser(action) {
   try {
    const result = yield call(saveUserAPI,action)
    if(result.data.code  === 1) {
        yield put({
            type: CREATED_SUCCESS,
            result,
          });
          return;
    }
    yield put({
        type: CREATED_FAIL
        
      });
   } catch (error) {
    yield put({
        type: CREATED_FAIL,
        error,
      });
   }
 }


 export function* watchSaveUser() {
    yield takeLatest(CREATED_SAVE,saveUser)
}


function* getAllUser(action){
    try {
      const getResult = yield call(getAllUserAPI,action)
      if(getResult.data.code === "1"){
        yield put({
          type: GET_SUCCESS,
          getResult
        });
        yield put({
          type: CREATEACCOUNT_RESET
        })
        yield put({
          type: UPDATEACCOUNT_RESET
        })
        yield put({
          type: DELETEACCOUNT_RESET
        })
        yield put({
          type: ACCOUNT_UPDATE_RESET
        })
        yield put({
          type: ACCOUNT_CREATED_RESET
        })
        return
      }
      yield put({
        type: GET_FAIL
        
      })
    } catch (error) {
      yield put({
        type: GET_FAIL,
        error,
      });
    }
}

export function* watchGetAllUser(){
  yield takeLatest(GET_SAVE,getAllUser)
}

function* createAccount(action) {
   try {
    const AccountResult = yield call(createAccountAPI,action)
    if(AccountResult.data.code  === 1) {
        yield put({
            type: CREATEACCOUNT_SUCCESS,
            AccountResult,
          });
          
          return;
    }
    yield put({
        type: CREATEACCOUNT_FAIL
        
      });
   } catch (error) {
    yield put({
        type: CREATEACCOUNT_FAIL,
        error,
      });
   }
 }

 export function* watchCreateAccount() {
  yield takeLatest(CREATEACCOUNT_SAVE,createAccount)
}

function* updateAccount(action) {
  console.log(11111111111)
   try {
    const result = yield call(updateAccountAPI,action)
    if(result.data.code  === 1) {
      console.log(9999999999999)
        yield put({
            type: UPDATEACCOUNT_SUCCESS
            
          });
          return;
    }
    yield put({
        type: UPDATEACCOUNT_FAIL
        
      });
   } catch (error) {
    yield put({
        type: UPDATEACCOUNT_FAIL,
        error,
      });
   }
 }

 export function* watchUpdateAccount() {
  yield takeLatest(UPDATEACCOUNT_SAVE,updateAccount)
}

function* deleteAccount(action) {
  
   try {
    const result = yield call(deleteAccountAPI,action)
    console.log(result)
    if(result.data.code  === 1) {
      console.log(1111232131)
        return yield put({
            type: ACCOUNT_DELETE_SUCCESS
          });
    }else {
      return yield put({
        type: ACCOUNT_DELETE_FAILURE
      });
    }
   } catch (error) {
    yield put({
      type: ACCOUNT_DELETE_FAILURE
      });
   }
 }

 export function* watchDeleteAccount() {
  yield takeLatest(ACCOUNT_DELETE,deleteAccount)
}

