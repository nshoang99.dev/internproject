
import {  all } from 'redux-saga/effects';
import {watchCreateAccount, watchGetAllUser, watchSaveUser, watchUpdateAccount,watchDeleteAccount} from './users';
import {watchGetAllDevice,watchCreateDevice,watchUpdateDevice,watchDeleteDevice} from './devices';
import {watchFetchAccountById, watchUpdateAccounts, watchCreatedAccount} from './account'
 export function* rootSaga() {
    // all thôi mọi người tự hiểu =))
    yield all([
        watchSaveUser(),
        watchGetAllUser(),
        watchCreateAccount(),
        watchUpdateAccount(),
        watchDeleteAccount(),
        watchGetAllDevice(),
        watchCreateDevice(),
        watchUpdateDevice(),
        watchDeleteDevice(),


        //
        watchFetchAccountById(),
        watchUpdateAccounts(),
        watchCreatedAccount()
    ]);
  }