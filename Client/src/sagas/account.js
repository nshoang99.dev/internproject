import { put, call, takeLatest } from 'redux-saga/effects';
import {ACCOUNT_FETCH,ACCOUNT_FAILURE,ACCOUNT_SUCCESS,
    ACCOUNT_UPDATE,ACCOUNT_UPDATE_FAILURE,ACCOUNT_UPDATE_SUCCESS,
    ACCOUNT_CREATED,ACCOUNT_CREATED_FAILURE,ACCOUNT_CREATED_SUCCESS
} from '../actions/account';
import { fetchById, updateById, created } from '../apis/account'


function* fetchAccountById(action) {
    try {
        const result = yield call(fetchById,action)
        if(result.data.code === "1") {
            yield put({
                type: ACCOUNT_SUCCESS,
                data: result.data.account
            })
        } else {
            yield put({
                type: ACCOUNT_FAILURE,
                data: {}
            })
        }
    } catch (error) {
        yield put({
            type: ACCOUNT_FAILURE,
            data: {}
        })
    }
}

export function* watchFetchAccountById() {
    return yield takeLatest(ACCOUNT_FETCH,fetchAccountById)
}

//update

function* updateAccount(action) {
    try {
        console.log('acction-saga',action)
        const result = yield call(updateById,action)
        console.log('result-saga',result)
        if(result.data.code === 1) {
            return yield put({
                type: ACCOUNT_UPDATE_SUCCESS
            })
        } else {
            return yield put({
                type: ACCOUNT_UPDATE_FAILURE
            })
        }
    } catch (error) {
        return yield put({
            type: ACCOUNT_UPDATE_FAILURE
        })
    }
}

export function* watchUpdateAccounts() {
    yield takeLatest(ACCOUNT_UPDATE,updateAccount)
}

///

function* createdAccount(action) {
    try {
        console.log('acction-saga',action)
        const result = yield call(created,action)
        console.log('result-saga',result)
        if(result.data.code === 1) {
            return yield put({
                type: ACCOUNT_CREATED_SUCCESS
            })
        } else {
            return yield put({
                type: ACCOUNT_CREATED_FAILURE
            })
        }
    } catch (error) {
        return yield put({
            type: ACCOUNT_CREATED_FAILURE
        })
    }
}

export function* watchCreatedAccount() {
    yield takeLatest(ACCOUNT_CREATED,createdAccount)
}


