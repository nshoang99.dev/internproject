import { getAllDeviceAPI } from '../apis/devices';
import { createDeviceAPI,updateDeviceAPI,deleteDeviceAPI } from '../apis/devices';
import { put, call, takeLatest } from 'redux-saga/effects';
import {GETDEVICE_SAVE,GETDEVICE_SUCCESS,GETDEVICE_FAIL} from "../actions/devices"
import { CREATEDEVICE_FAIL,CREATEDEVICE_SAVE,CREATEDEVICE_SUCCESS} from "../actions/devices";
import { UPDATEDEVICE_FAIL,UPDATEDEVICE_SAVE,UPDATEDEVICE_SUCCESS} from "../actions/devices";
import { DELETEDEVICE_FAIL,DELETEDEVICE_SAVE,DELETEDEVICE_SUCCESS} from "../actions/devices";

function* getAllDevice(action){
    console.log('actions-saga')
      console.log("actions-saga",action)
      try {
        const getResult = yield call(getAllDeviceAPI,action)
        console.log("result-saga",getResult)
        if(getResult.data.code === "1"){
          yield put({
            type: GETDEVICE_SUCCESS,
            getResult
          });
          return
        }
        yield put({
          type: GETDEVICE_FAIL
          
        })
      } catch (error) {
        yield put({
          type: GETDEVICE_FAIL,
          error,
        });
      }
  }
  
  export function* watchGetAllDevice(){
    yield takeLatest(GETDEVICE_SAVE,getAllDevice)
  }

  function* createDevice(action) {
    try {
     const DeviceResult = yield call(createDeviceAPI,action)
     if(DeviceResult.data.code  === 1) {
         yield put({
             type: CREATEDEVICE_SUCCESS,
             DeviceResult,
           });
           return;
     }
     yield put({
         type: CREATEDEVICE_FAIL
         
       });
    } catch (error) {
     yield put({
         type: CREATEDEVICE_FAIL,
         error,
       });
    }
  }
 
  export function* watchCreateDevice() {
   yield takeLatest(CREATEDEVICE_SAVE,createDevice)
 }

 function* updateDevice(action) {
  try {
   const result = yield call(updateDeviceAPI,action)
   if(result.data.code  === 1) {
       yield put({
           type: UPDATEDEVICE_SUCCESS,
           result,
         });
         return;
   }
   yield put({
       type: UPDATEDEVICE_FAIL
       
     });
  } catch (error) {
   yield put({
       type: UPDATEDEVICE_FAIL,
       error,
     });
  }
}

export function* watchUpdateDevice() {
 yield takeLatest(UPDATEDEVICE_SAVE,updateDevice)
}

function* deleteDevice(action) {
   try {
    const result = yield call(deleteDeviceAPI,action)
    if(result.data.code  === 1) {
        yield put({
            type: DELETEDEVICE_SUCCESS,
            result,
          });
          return;
    }
    yield put({
        type: DELETEDEVICE_FAIL
        
      });
   } catch (error) {
    yield put({
        type: DELETEDEVICE_FAIL,
        error,
      });
   }
 }

 export function* watchDeleteDevice() {
  yield takeLatest(DELETEDEVICE_SAVE,deleteDevice)
}