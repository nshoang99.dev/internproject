import axios from 'axios'

export const getAllDeviceAPI = (action)=>{
    return axios.get(`http://localhost:4000/devices/get-alldevice`)
}

export const createDeviceAPI = (action)=>{
    return axios.post(`http://localhost:4000/devices/create-device`,action)
}

export const updateDeviceAPI = (action)=>{
    return axios.post(`http://localhost:4000/devices/update-device`,action)
}

export const deleteDeviceAPI = (action)=>{
    return axios.post(`http://localhost:4000/devices/delete-device`,action)
}