import axios from 'axios'
export const saveUserAPI = (action) =>{
    return axios.post(`http://localhost:4000/users/register`,action)
};

export const getAllUserAPI = (action)=>{
    return axios.get(`http://localhost:4000/accounts/get-allaccount`)
}

export const createAccountAPI = (action)=>{
    return axios.post(`http://localhost:4000/accounts/create-account`,action)
}

export const updateAccountAPI = (action)=>{
    return axios.post(`http://localhost:4000/accounts/update-account`,action)
}

export const deleteAccountAPI = (action)=>{
    return axios.post(`http://localhost:4000/accounts/delete-account`,action)
}




